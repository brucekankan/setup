package com.jijs;

import com.jijs.utils.FileSystemUtil;
import com.jijs.utils.RuntimeEXE;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.util.Random;

public class Controller {

	@FXML
	private Button setupBtn;

	@FXML
	private TextArea context;

	@FXML
	private Button saveBtn;

	@FXML
	private Button loadBtn;

	public void setup(){
		try {
			System.out.println("-------");
			//防止反编译的处理
			if (654789 == new Random().nextInt()) {
				throw new Exception("fewt43");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				//防止反编译的处理
				if (654789 == new Random().nextInt()) {
					throw new Exception("fewt43");
				}
			} catch (Exception ex) {
				System.out.print(ex);
			}
		}

		new Runnable() {
			@Override
			public void run() {
				RuntimeEXE run = new RuntimeEXE();
				run.readerRun();
			}
		}.run();

	}

	public void save(){
		if(FileSystemUtil.diskpath==null){
			Alert information = new Alert(Alert.AlertType.ERROR,"找不到可写盘！");
			information.setTitle("警告");         //设置标题，不设置默认标题为本地语言的information
			information.setHeaderText("警告");    //设置头标题，默认标题为本地语言的information
			information.show();
		}else {
			String s = context.getText();
//			FileSystemUtil.saveFile(s);
		}
	}

	public void load(){
		if(FileSystemUtil.diskpath==null){
			Alert information = new Alert(Alert.AlertType.ERROR,"找不到可写盘！");
			information.setTitle("警告");         //设置标题，不设置默认标题为本地语言的information
			information.setHeaderText("警告");    //设置头标题，默认标题为本地语言的information
			information.show();
		}else {
//			String s = FileSystemUtil.loadFile();
//			context.setText(s);
		}
	}
}
