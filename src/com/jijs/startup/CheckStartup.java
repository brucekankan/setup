package com.jijs.startup;

import com.jijs.utils.RuntimeEXE;

import java.util.Random;

/**
 * Created by cc on 2018/1/22.
 */
public class CheckStartup {
	public static void main(String[] args) {
		try {
			System.out.println("-------");
			//防止反编译的处理
			if (654789 == new Random().nextInt()) {
				throw new Exception("fewt43");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				//防止反编译的处理
				if (654789 == new Random().nextInt()) {
					throw new Exception("fewt43");
				}
			} catch (Exception ex) {
				System.out.print(ex);
			}
		}
		new Runnable() {
			@Override
			public void run() {
				RuntimeEXE runtimeEXE = new RuntimeEXE();
				runtimeEXE.checkRun();
			}
		}.run();

	}
}
