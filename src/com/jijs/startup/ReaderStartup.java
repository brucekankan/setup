package com.jijs.startup;

import com.jijs.http.HttpServer;
import com.jijs.utils.RuntimeEXE;

import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;

/**
 * Created by cc on 2018/1/22.
 */
public class ReaderStartup {

	public static void main(String[] args) {
		try {
			System.out.println("-------");
			//防止反编译的处理
			if (654789 == new Random().nextInt()) {
				throw new Exception("fewt43");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				//防止反编译的处理
				if (654789 == new Random().nextInt()) {
					throw new Exception("fewt43");
				}
			} catch (Exception ex) {
				System.out.print(ex);
			}
		}

		RuntimeEXE runtimeEXE = new RuntimeEXE();
		runtimeEXE.readerRun();

	}
}
