package com.jijs.utils;

import java.util.Random;

public class Tools {

	public static void main(String[] args) {
		try {
			System.out.println("-------");
			//防止反编译的处理
			if (654789 == new Random().nextInt()) {
				throw new Exception("fewt43");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				//防止反编译的处理
				if (654789 == new Random().nextInt()) {
					throw new Exception("fewt43");
				}
			} catch (Exception ex) {
				System.out.print(ex);
			}
		}
		new Runnable() {
			@Override
			public void run() {
				System.out.println(FileSystemUtil.diskpath);
			}
		}.run();
	}
	
	public static boolean isBlank(String str) {
        if (str == null || str.trim().length() == 0) {
            return true;
        }
        return false;
    }
	
	public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }
}
