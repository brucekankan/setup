package com.jijs.utils;

import com.jijs.http.HttpServer;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

public class RuntimeEXE {

	public static void main(String[] args)  {
		boolean b = isReadOnly();
		System.out.println(b);
	}
	
	public static File getRootDirs() {
        File f = new File(System.getProperty("user.dir"));
        f = getRootDir(f);
        return f;
    }
	
	private static File getRootDir(File f) {
        if(f.getParentFile()==null){
            return f;
        }
        return getRootDir(f.getParentFile());
    }
	
	private static String getProjectRootPath() {
		File f = getRootDirs();

		return f.getAbsolutePath()+File.separator+"234982a7-41c6-4beb-08e5-fe50e8b54a328"+File.separator+"02cf443a-f2df-4ef3-90bf-9f7340da245702cf443a-f2df-4ef3-90bf-9f7340da245702cf443a-f2df-4ef3-90bf-9f7340da2457"+File.separator;
	}

	private String getReaderFile() {
		return getProjectRootPath()+"藏研版藏文大藏经.exe";
	}

	private String getCheckFile() {
		return getProjectRootPath()+"checkfile_t.exe";
	}

	public static boolean isReadOnly() {
		File f = new File(getProjectRootPath() + UUID.randomUUID().toString());
		try {
			if (f.createNewFile()) {
				f.delete();
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}


	public void checkRun() {
		try {
			System.out.println("-------");
			//防止反编译的处理
			if (654789 == new Random().nextInt()) {
				throw new Exception("fewt43");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				//防止反编译的处理
				if (654789 == new Random().nextInt()) {
					throw new Exception("fewt43");
				}
			} catch (Exception ex) {
				System.out.print(ex);
			}
		}
		new Runnable() {
			@Override
			public void run() {
				try {
					if(FileSystemUtil.diskpath!=null && isReadOnly()) {
						Runtime.getRuntime().exec(getCheckFile());
					}else{
						System.out.println("磁盘格式不正确");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.run();

	}
	
	public void readerRun() {
		try {
			System.out.println("-------");
			//防止反编译的处理
			if (654789 == new Random().nextInt()) {
				throw new Exception("fewt43");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				//防止反编译的处理
				if (654789 == new Random().nextInt()) {
					throw new Exception("fewt43");
				}
			} catch (Exception ex) {
				System.out.print(ex);
			}
		}
		new Runnable() {
			@Override
			public void run() {
				try {
					if(FileSystemUtil.diskpath!=null && isReadOnly()) {
						Thread t = new Thread(()->{
							try {
								HttpServer webserver = new HttpServer();
								webserver.start();
							} catch (IOException ex) {
								ex.printStackTrace();
							}
						});
						t.setDaemon(true);
						t.start();


						Process p = Runtime.getRuntime().exec(getReaderFile());
						try {
							p.waitFor();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						System.exit(0);
						System.out.println("ok");
					}else{
						System.out.println("磁盘格式不正确");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.run();



	}
}
