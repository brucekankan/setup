package com.jijs.utils;

import java.io.*;
import java.util.Random;

/**
 * Created by cc on 2018/1/8.
 */
public class FileSystemUtil {

	//当前程序对外提供的接口服务，服务的端口写在 当前用户的临时目录下，文件的名字为下面的，不能修改，里面写的是端口号
	private final static String port_file="ade02fb248374f54a765d3bb2ea42198.tmp";

	private final static long GB = 1024 * 1024 * 1024L;
	private final static long MB = 1024 * 1024L;

	private final static long MIN_CAPACITY = 2 * GB + 512 * MB;
	private final static long MAX_CAPACITY = 3 * GB + 512 * MB;

	private final static String verifyDir = "TripitakaReader";
	private final static String verifyFile = "TripitakaReader"+File.separator+"seq";

	//获取可写目录地址
	public static File diskpath;

	static {
		try {
			System.out.println("-------");
			//防止反编译的处理
			if (654789 == new Random().nextInt()) {
				throw new Exception("fewt43");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				//防止反编译的处理
				if (654789 == new Random().nextInt()) {
					throw new Exception("fewt43");
				}
			} catch (Exception ex) {
				System.out.print(ex);
			}
		}

		File[] roots = File.listRoots();

		boolean flag = false;

		for (int i = roots.length-1; i >=0 ; i--) {
			File file = new File(roots[i]+verifyFile);
			if(file.exists()) {
				if (MIN_CAPACITY < roots[i].getTotalSpace() && roots[i].getTotalSpace() < MAX_CAPACITY) {
					diskpath = new File(roots[i]+verifyDir);
					flag = true;
					break;
				}

			}
		}

		if(!flag) {
			for (int i = roots.length-1; i >=0 ; i--) {
				File file = new File(roots[i]+verifyDir);
				if(file.exists()) {
					if (MIN_CAPACITY < roots[i].getTotalSpace() && roots[i].getTotalSpace() < MAX_CAPACITY) {
						diskpath = file;
						flag = true;
						break;
					}
				}
			}
		}

	}

	public static void main(String[] args) {
		new Runnable() {
			@Override
			public void run() {
				System.out.println(FileSystemUtil.diskpath);
			}
		}.run();

	}

	/**
	 * 保存当前服务的端口信息到 当前系统用户的临时目录下的 【port_file="ade02fb248374f54a765d3bb2ea42198.tmp"】 文件中
	 * @param port
	 * @return
	 */
	public static boolean savePort(int port){
		File portFile = new File(System.getProperty("java.io.tmpdir"), port_file);
		try (Writer writer = new FileWriter(portFile)){
			writer.write(String.valueOf(port));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/*public static boolean saveFile(String context) {

		try (FileOutputStream writerStream = new FileOutputStream(file);
			 BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(writerStream, "UTF-8"));) {

			writer.write(context);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String loadFile() {
		try (FileInputStream fileInputStream = new FileInputStream(file);
			 InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
			 BufferedReader bufferedReader = new BufferedReader(inputStreamReader);) {

			StringBuffer sb = new StringBuffer();
			String text = null;
			while ((text = bufferedReader.readLine()) != null) {
				sb.append(text);
			}
			return sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}*/
}
