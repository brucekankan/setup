package cn.com.infcn.test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;

/**
 * Created by cc on 2017/12/19.
 */
public class Test {


	public static void main(String[] args) {
//		File[] roots = File.listRoots();
//		for (int i = 0; i < roots.length; i++) {
//			System.out.println(roots[i]);
//			System.out.println("Free space = \t" + roots[i].getFreeSpace()*1.0/1024/1024/1024);
//			System.out.println("Usable space = \t" + roots[i].getUsableSpace()*1.0/1024/1024/1024);
//			System.out.println("Total space = \t" + roots[i].getTotalSpace()*1.0/1024/1024/1024);
//			System.out.println();
//		}

		File[] roots = File.listRoots();
		for (File f: roots) {
			FileSystemView fileSys=FileSystemView.getFileSystemView();
			System.out.println("=="+fileSys.getSystemDisplayName(f));
		}

	}
}
